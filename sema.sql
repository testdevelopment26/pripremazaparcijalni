-- MySQL dump 10.13  Distrib 5.7.9, for linux-glibc2.5 (x86_64)
--
-- Host: localhost    Database: priprema_za_pacijalni
-- ------------------------------------------------------
-- Server version	5.6.33-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Drzava`
--

DROP TABLE IF EXISTS `Drzava`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Drzava` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `naziv` varchar(45) NOT NULL,
  `code` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Drzava`
--

LOCK TABLES `Drzava` WRITE;
/*!40000 ALTER TABLE `Drzava` DISABLE KEYS */;
INSERT INTO `Drzava` VALUES (1,'Srbija',381),(2,'SAD',1),(3,'Nemacka',44);
/*!40000 ALTER TABLE `Drzava` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Film`
--

DROP TABLE IF EXISTS `Film`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Film` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `naziv` varchar(45) NOT NULL,
  `godina` int(11) NOT NULL,
  `zanr` varchar(45) NOT NULL,
  `drzavaPorekla` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_Film_1_idx` (`drzavaPorekla`),
  CONSTRAINT `fk_Film_1` FOREIGN KEY (`drzavaPorekla`) REFERENCES `Drzava` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Film`
--

LOCK TABLES `Film` WRITE;
/*!40000 ALTER TABLE `Film` DISABLE KEYS */;
INSERT INTO `Film` VALUES (1,'Vruc Vetar',1980,'komedija',1),(2,'Shawkshank Redemption',1992,'drama',2),(3,'Home Alone',2003,'komedija',2),(4,'Die Welle',2002,'akcija',3),(5,'Zona 2.0',2023,'komedija',1);
/*!40000 ALTER TABLE `Film` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `FilmGlumac`
--

DROP TABLE IF EXISTS `FilmGlumac`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `FilmGlumac` (
  `idFilm` int(11) NOT NULL,
  `idGlumac` int(11) NOT NULL,
  KEY `idFilm_idx` (`idFilm`),
  KEY `idGlumac_idx` (`idGlumac`),
  CONSTRAINT `idFilm` FOREIGN KEY (`idFilm`) REFERENCES `Film` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `idGlumac` FOREIGN KEY (`idGlumac`) REFERENCES `Glumac` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `FilmGlumac`
--

LOCK TABLES `FilmGlumac` WRITE;
/*!40000 ALTER TABLE `FilmGlumac` DISABLE KEYS */;
INSERT INTO `FilmGlumac` VALUES (1,1),(1,2),(2,3),(2,4),(3,5),(4,6),(1,4);
/*!40000 ALTER TABLE `FilmGlumac` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Glumac`
--

DROP TABLE IF EXISTS `Glumac`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Glumac` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ime` varchar(45) NOT NULL,
  `prezime` varchar(45) NOT NULL,
  `godinaRodjenja` int(11) NOT NULL,
  `pol` varchar(45) NOT NULL,
  `drzavljanin` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `drzavljanin_idx` (`drzavljanin`),
  CONSTRAINT `drzavljanin` FOREIGN KEY (`drzavljanin`) REFERENCES `Drzava` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Glumac`
--

LOCK TABLES `Glumac` WRITE;
/*!40000 ALTER TABLE `Glumac` DISABLE KEYS */;
INSERT INTO `Glumac` VALUES (1,'Ljubisa','Samardzic',1950,'muski',1),(2,'Neda','Arneric',1963,'zenski',1),(3,'Will','Smith',1973,'muski',2),(4,'Brad','Pitt',1975,'muski',2),(5,'Emily','Clarke',1988,'zenski',2),(6,'Daniel','Bruhl',1985,'muski',3),(7,'Pera','Peric',2001,'muski',1);
/*!40000 ALTER TABLE `Glumac` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2024-05-08 19:43:51
