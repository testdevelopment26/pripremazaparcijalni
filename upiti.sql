-- Upit 1
-- SELECT * FROM Film WHERE zanr = 'komedija';
SELECT naziv FROM Film WHERE zanr = 'komedija';

-- Upit 2
-- SELECT * FROM Glumac WHERE godinaRodjenja < 1994;
SELECT ime, prezime FROM Glumac WHERE godinaRodjenja < 1994;

-- Upit 3
SELECT * 
FROM Glumac
JOIN Drzava
ON Glumac.drzavljanin = Drzava.id
WHERE Glumac.pol = 'zenski' AND Drzava.naziv = 'SAD';

-- Upit 4
SELECT Film.naziv
FROM Film
JOIN Drzava
ON Film.drzavaPorekla = Drzava.id
WHERE Film.godina > 2021 AND Drzava.naziv = 'Srbija';

-- Upit 5
SELECT Drzava.Naziv, COUNT(Film.id) AS BrojFilmova
FROM Drzava
JOIN Film
ON Film.drzavaPorekla = Drzava.id
GROUP BY Drzava.Naziv;

-- UPIT 6
SELECT DISTINCT Film.Naziv
FROM Film
JOIN FilmGlumac
ON Film.id = FilmGlumac.idFilm
JOIN Glumac
ON Glumac.id = FilmGlumac.idGlumac
WHERE Film.drzavaPorekla = Glumac.drzavljanin;




